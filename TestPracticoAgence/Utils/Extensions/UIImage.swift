//
//  UIImage.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 23/5/22.
//

import UIKit

extension UIImage {
    func imageWith(newSize: CGFloat) -> UIImage {
        let size = CGSize(width: newSize, height: newSize)
        let image = UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }

        return image.withRenderingMode(renderingMode)
    }
}
