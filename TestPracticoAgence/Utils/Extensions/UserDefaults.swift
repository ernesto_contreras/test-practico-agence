//
//  UserDefaults.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import Foundation

enum UserDefaultKeys: String, CaseIterable {
    case user
}

extension UserDefaults {
    var user: User? {
            get {
                guard let data = data(forKey: UserDefaultKeys.user.rawValue) else { return nil }
                do {
                    let decoder = JSONDecoder()
                    return try decoder.decode(User.self, from: data)
                } catch {
                    return nil
                }
            }
            set {
                do {
                    let data = try JSONEncoder().encode(newValue)
                    set(data, forKey: UserDefaultKeys.user.rawValue)
                } catch {
                    set(nil, forKey: UserDefaultKeys.user.rawValue)
                }
            }
        }
}
