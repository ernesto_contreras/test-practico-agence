//
//  NotificationCenter.Name.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 25/5/22.
//

import Foundation

extension Notification.Name {
    static let didBuyProduct = Notification.Name(rawValue: "didBuyProduct")
}
