//
//  String.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import Foundation
import GoogleSignIn

class Constant {
    static let userPlaceholder = "Escribe tu nombre de usuario"
    static let passwordPlaceholder = "Escribe tu contraseña"
    static let defaultEmail = "randomuser@gmail.com"
    static let googleLogin = "Iniciar sesión con Google"
    static let facebookLogin = "Iniciar sesión con Facebook"
    static let simpleLogin = "Iniciar sesión"
    static let signInConfig = GIDConfiguration(clientID: "862163111743-4olrp8bfvns411plorf2m3crkpk387e5.apps.googleusercontent.com")
    static let unsplashURL = "https://api.unsplash.com/photos/random"
    static let productDescription = "Este producto es super especial porque se le puede sacar provecho para muchas cosas."
    static let notAuthorized = "Si quieres ver tu ubicación, dale permisos de localización a TestPracticoAgence"
    static let googleApiKey = "AIzaSyDnnImaSdtaD1K4ROD-abImc_fIhtA1VCA"
    static let facebookCancel = "Cancelaste el inicio de sesión con Facebook."
    static let buyProduct = "Comprar producto"
    static let buy = "Comprar"
    static let cancel = "Cancelar"
    static let goToSettings = "Ve a los ajustes"
    static let forgotPassword = "¿Olvidaste tu contraseña?"
    static let productBought = "Tu producto fue comprado exitosamente."
}

extension String {

    var isValidUsername: Bool {
        return self != Constant.userPlaceholder && !self.isEmpty
    }

    var isValidPassword: Bool {
        return self != Constant.passwordPlaceholder && !self.isEmpty
    }
}
