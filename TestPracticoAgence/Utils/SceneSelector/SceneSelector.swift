//
//  SceneSelector.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import UIKit
import SideMenuSwift

class SceneSelector {
    static var shared = SceneSelector()

    private var window: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }

    func setInitialScene() {
        guard let window = self.window else {
            return
        }
        
        if UserDefaults.standard.user != nil {
            let productListViewController = setScene(
                fromViewController: ProductListViewController.self,
                inNavigationController: true
            )
            window.rootViewController = productListViewController
        } else {
            let loginViewController = setScene(fromViewController: LoginViewController.self)
            window.rootViewController = loginViewController
        }
        

        UIView.transition(with: window,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }

    func moveScene<T: UIViewController>(controller: T.Type) {
        guard let window = self.window else {
            return
        }

        let newVC = setScene(fromViewController: controller, inNavigationController: true)
        window.rootViewController = newVC

        UIView.transition(with: window,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }

    // MARK: - Helper
    private func setScene<T: UIViewController>(fromViewController vc: T.Type, inNavigationController showNavigationController: Bool = false) -> UIViewController {
        let nextViewController: UIViewController

        if showNavigationController {
            nextViewController = UINavigationController(
                rootViewController: T.instantiateFromNib()
            )
        } else {
            nextViewController = T.instantiateFromNib()
        }

        let menuViewController = SideMenuController(contentViewController: nextViewController, menuViewController: MenuViewController())
        
        return menuViewController
    }
}
