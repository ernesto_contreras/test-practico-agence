//
//  TPAButton.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import UIKit

class TPAButton: UIButton {

    // MARK: - Style
    enum Style {
        case googleLogin
        case facebookLogin
        case simpleLogin
        case forgotPassword
        case cancel
        case accept
        case buy
        case goToSettings
    }

    // MARK: - Properties
    private var activityIndicator: UIActivityIndicatorView!
    private var originalButtonText: String?

    var style: Style = .simpleLogin {
        didSet {
            setStyle()
            setImage()
        }
    }

    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.height / 2
    }

    // MARK: - Setup
    private func initialize() {
        backgroundColor = UIColor(named: "mainWhite")
        layer.masksToBounds = false
        clipsToBounds = true
        titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        setTitleColor(.darkGray, for: .normal)
    }

    // MARK: - Private
    private func setStyle() {
        switch style {
        case .googleLogin:
            setTitle(Constant.googleLogin, for: .normal)
        case .facebookLogin:
            setTitle(Constant.facebookLogin, for: .normal)
        case .simpleLogin:
            setTitle(Constant.simpleLogin, for: .normal)
            setTitleColor(.white, for: .normal)
            backgroundColor = .darkGray
        case .forgotPassword:
            backgroundColor = .clear
            setTitle(Constant.forgotPassword, for: .normal)
            underLineText()
        case .cancel:
            backgroundColor = UIColor(named: "mainGray")
        case .accept:
            backgroundColor = .darkGray
            setTitleColor(UIColor(named: "mainGray"), for: .normal)
        case .buy:
            backgroundColor = .darkGray
            setTitle(Constant.buyProduct, for: .normal)
            setTitleColor(UIColor(named: "mainWhite"), for: .normal)
        case .goToSettings:
            backgroundColor = .darkGray
            setTitle(Constant.goToSettings, for: .normal)
            setTitleColor(UIColor(named: "mainWhite"), for: .normal)
        }
    }

    private func setImage() {
        switch style {
        case .googleLogin:
            addImage(named: "google")
        case .facebookLogin:
            addImage(named: "facebook")
        default:
            break
        }
    }

    private func addImage(named name: String) {
        let image = UIImage(named: name)
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: 32, y: 12, width: 24, height: 24)
        imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
    }
}

// MARK: - ActivityIndicator
extension TPAButton {
    func showLoading() {
        originalButtonText = self.titleLabel?.text
        self.setTitle("", for: .normal)

        if activityIndicator == nil {
            activityIndicator = createActivityIndicator()
        }

        showSpinning()
    }

    func hideLoading() {
        self.setTitle(originalButtonText, for: .normal)
        activityIndicator.stopAnimating()
        self.isUserInteractionEnabled = true
    }

    private func createActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .black
        return activityIndicator
    }

    private func showSpinning() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
    }

    private func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)

        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }

    private func underLineText() {
        guard let title = title(for: .normal) else { return }

        let titleString = NSMutableAttributedString(string: title)
        titleString.addAttribute(
            .underlineStyle,
            value: NSUnderlineStyle.single.rawValue,
            range: NSRange(location: 0, length: title.count)
        )
        setAttributedTitle(titleString, for: .normal)
    }
}
