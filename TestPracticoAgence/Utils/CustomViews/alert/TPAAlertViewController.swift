//
//  TPAAlertView.swift
//  hopnow
//
//  Created by Leyner Castillo on 18/05/21.
//

import UIKit

protocol TPAAlertViewControllerDelegate: AnyObject {
    func alertViewController(_ viewController: TPAAlertViewController, okTouched button: UIButton)
}

enum AlertType {
    case autoDismissable
    case buy
    case acceptOnly
}

class TPAAlertViewController: UIViewController {

    // MARK: - Outlet
    @IBOutlet private(set) weak var messageLabel: UILabel!
    @IBOutlet private(set) weak var contentView: UIView!
    @IBOutlet private(set) weak var cancelButton: TPAButton!
    @IBOutlet private(set) weak var okButton: TPAButton!
    @IBOutlet private(set) weak var buttonsStackView: UIStackView!
    
    // MARK: - Properties
    var alertType: AlertType = .autoDismissable
    var message: String?
    var cancelTitle: String?
    var okTitle: String?
    weak var delegate: TPAAlertViewControllerDelegate?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Setup
    private func setup() {
        contentView.layer.cornerRadius = 8
        contentView.layer.masksToBounds = true
        messageLabel.text = message
        setupContent()
    }

    private func setupContent() {
        switch alertType {
        case .autoDismissable:
            buttonsStackView.isHidden = true
        case .buy:
            okButton.style = .accept
            okButton.setTitle(Constant.buy, for: .normal)
            cancelButton.style = .cancel
            cancelButton.setTitle(Constant.cancel, for: .normal)
        case .acceptOnly:
            cancelButton.isHidden = true
            okButton.style = .goToSettings
        }
    }

    // MARK: - Public
    func setAlert(titleMessage: String?, type: AlertType) {
        self.message = titleMessage
        self.alertType = type
        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .crossDissolve
    }

    func dismissAlert() {
        Timer.scheduledTimer(withTimeInterval: 0.7, repeats: false) { _ in
            self.dismiss(animated: true)
        }
    }

    // MARK: - Action
    @IBAction private func didTapBackgroundButton(_ sender: UIButton) {
        self.dismiss(animated: true)
    }

    @IBAction private func didTapOkButton(_ sender: UIButton) {
        delegate?.alertViewController(self, okTouched: sender)
    }
}
