//
//  TPAViewController.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 23/5/22.
//

import UIKit
import SideMenuSwift

class TPAViewController: UIViewController {

    // MARK: - Properties
    private var activityIndicatorView: UIActivityIndicatorView?
    var hasMenu = false {
        didSet {
            SideMenuController.preferences.basic.enablePanGesture = hasMenu
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Setup
    private func setup() {
        if navigationItem.leftBarButtonItem == nil {
            let image = UIImage(named: "menu")?.imageWith(newSize: 24)
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(didTapMenuButton))
            navigationItem.leftBarButtonItem?.tintColor = .darkGray
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }

    func showActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .medium)
        activityIndicatorView?.color = .darkGray
        let frameSize: CGPoint = CGPoint(
            x: UIScreen.main.bounds.size.width * 0.5,
            y: UIScreen.main.bounds.size.height * 0.5
        )
        activityIndicatorView?.center = frameSize
        self.view.addSubview(activityIndicatorView!)
        self.view.bringSubviewToFront(activityIndicatorView!)
        activityIndicatorView?.startAnimating()
    }

    func hideActivityIndicator() {
        if activityIndicatorView != nil {
            activityIndicatorView?.stopAnimating()
            activityIndicatorView = nil
        }
    }
    
    @objc func didTapMenuButton() {
        sideMenuController?.revealMenu()
    }
}
