//
//  AppDelegate.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import UIKit
import FBSDKCoreKit
import GoogleSignIn
import GoogleMaps

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    func application(_ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        ApplicationDelegate.shared.application(
            application, didFinishLaunchingWithOptions: launchOptions
        )

        GMSServices.provideAPIKey(Constant.googleApiKey)
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:] ) -> Bool {
        let facebookDidHandle = ApplicationDelegate.shared.application(
            app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )

        let googleDidHandle = GIDSignIn.sharedInstance.handle(url)

        return facebookDidHandle || googleDidHandle
    }
}

