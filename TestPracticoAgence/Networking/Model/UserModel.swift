//
//  UserModel.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import Foundation

struct User: Codable {
    var name: String?
    var email: String?
}
