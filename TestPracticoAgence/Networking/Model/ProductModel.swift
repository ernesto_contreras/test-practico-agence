//
//  ProductModel.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 25/5/22.
//

import Foundation

struct Product {
    var name: String
    var image: Image?
    var id: String = UUID().uuidString
}

struct Image: Codable {
    var id: String
    var urls: ImageURL
}

struct ImageURL: Codable {
    var thumb: String
}
