//
//  ProductDetailViewController.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import UIKit
import Kingfisher
import GoogleMaps

class ProductDetailViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet private(set) weak var mapView: GMSMapView!
    @IBOutlet private(set) weak var containerView: UIView!
    @IBOutlet private(set) weak var productImageView: UIImageView!
    @IBOutlet private(set) weak var productNameLabel: UILabel!
    @IBOutlet private(set) weak var productDescriptionLabel: UILabel!
    @IBOutlet private(set) weak var buyButton: TPAButton!
    
    // MARK: - Properties
    var product: Product?
    private var locationManager = CLLocationManager()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
    }
    
    // MARK: - Setup
    private func setup() {
        self.setupUI()
    }

    private func setupUI() {
        navigationController?.navigationBar.tintColor = .darkGray
        if let product = product {
            if let img = product.image?.urls.thumb, let url = URL(string: img) {
                self.productImageView.kf.indicatorType = .activity
                self.productImageView.kf.setImage(with: url)
            }
        }
        productNameLabel.text = product?.name
        productDescriptionLabel.text = Constant.productDescription
        containerView.layer.cornerRadius = 22
        productImageView.layer.cornerRadius = 22
        buyButton.style = .buy
    }

    private func updateMap(_ coordinates: CLLocationCoordinate2D) {
        
        let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: 15)
        let cameraUpdate = GMSCameraUpdate.setCamera(camera)
        mapView.moveCamera(cameraUpdate)
        mapView.isUserInteractionEnabled = false

        mapView.clear()
        let marker = GMSMarker()
        marker.position = coordinates
        marker.map = mapView
    }

    @IBAction private func buyButtonTapped(_ sender: TPAButton) {
        presentAlert(title: "¿Estás seguro de que quieres comprar este producto?", type: .buy)
    }
}

extension ProductDetailViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        updateMap(location.coordinate)
    }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .denied, .restricted:
            presentAlert(title: Constant.notAuthorized, type: .acceptOnly)
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        default:
            break
        }
    }
    
    private func presentAlert(title: String, type: AlertType) {
        let alertVC = TPAAlertViewController()
        alertVC.setAlert(titleMessage: title, type: type)
        alertVC.delegate = self
        self.present(alertVC, animated: true)
    }
}

extension ProductDetailViewController: TPAAlertViewControllerDelegate {
    func alertViewController(_ viewController: TPAAlertViewController, okTouched button: UIButton) {
        switch viewController.alertType {
        case .buy:
            SceneSelector.shared.moveScene(controller: ProductListViewController.self)
            NotificationCenter.default.post(name: .didBuyProduct, object: nil)
        case .acceptOnly:
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        default:
            break
        }
    }
}
