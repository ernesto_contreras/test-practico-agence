//
//  LoginViewController.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import UIKit
import SideMenuSwift

protocol LoginViewModelDelegate: AnyObject {
    func didLoginSuccessfully()
    func loginFailed(message: String?)
}

class LoginViewController: TPAViewController {

    // MARK: - Outlet
    @IBOutlet private(set) weak var scrollView: UIScrollView!
    @IBOutlet private(set) weak var userNameTextView: TPATextFieldView!
    @IBOutlet private(set) weak var passwordTextView: TPATextFieldView!
    @IBOutlet private(set) weak var googleButton: TPAButton!
    @IBOutlet private(set) weak var facebookButton: TPAButton!
    @IBOutlet private(set) weak var loginButton: TPAButton!
    @IBOutlet private(set) weak var shopImageView: UIImageView!
    @IBOutlet private(set) weak var forgotPasswordButton: TPAButton!
    @IBOutlet private(set) weak var usernameHeightConstraint: NSLayoutConstraint!
    @IBOutlet private(set) weak var passwordHeightConstraint: NSLayoutConstraint!
    
    lazy var viewModel: LoginViewModel = {
       let vm = LoginViewModel()
        vm.delegate = self
        return vm
    }()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        shopImageView.layer.cornerRadius = shopImageView.frame.height / 2
    }
    
    // MARK: - Setup
    private func setup() {
        setupObserver()
        setupUI()
    }

    private func setupObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func setupUI() {
        setupViews()
        setupTextViews()
        setupButtons()
    }

    private func setupViews() {
        SideMenuController.preferences.basic.enablePanGesture = false
        navigationController?.setNavigationBarHidden(true, animated: false)
        view.backgroundColor = .white
    }

    private func setupTextViews() {
        userNameTextView.textType = .username
        passwordTextView.textType = .password
    }

    private func setupButtons() {
        googleButton.style = .googleLogin
        facebookButton.style = .facebookLogin
        loginButton.style = .simpleLogin
        forgotPasswordButton.style = .forgotPassword
    }
    
    // MARK: - Helpers

    // MARK: - Actions
    @IBAction private func loginButtonTapped(_ sender: TPAButton) {
        showActivityIndicator()
        viewModel.handleRegularLogin(
            password: passwordTextView.textField.text, userName: userNameTextView.textField.text
        )
    }
    
    @IBAction private func facebookButtonTapped(_ sender: TPAButton) {
        showActivityIndicator()
        viewModel.fetchUserDataFacebook(controller: self)
    }
    
    @IBAction private func googleButtonTapped(_ sender: TPAButton) {
        showActivityIndicator()
        viewModel.fetchUserDataGoogle(controller: self)
    }
    
    @IBAction private func forgotPasswordTapped(_ sender: UIButton) {
        
    }

    @objc private func keyboardWillShow(notification:NSNotification) {
        guard let keyboardSize = (
            notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        )?.cgRectValue else {
            return
        }
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height + 80, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc private func keyboardWillHide(notification:NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
}

extension LoginViewController: LoginViewModelDelegate {
    func didLoginSuccessfully() {
        hideActivityIndicator()
        SceneSelector.shared.moveScene(controller: ProductListViewController.self)
    }
    
    func loginFailed(message: String?) {
        hideActivityIndicator()
        let alertVC = TPAAlertViewController()
        alertVC.setAlert(titleMessage: message, type: .autoDismissable)
        self.present(alertVC, animated: true) {
            alertVC.dismissAlert()
        }
    }
}
