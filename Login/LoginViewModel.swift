//
//  LoginViewModel.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import Foundation
import FBSDKLoginKit
import GoogleSignIn

class LoginViewModel {
    private enum LoginType {
        case google
        case facebook
        case simple
    }

    private var user: User? {
        didSet {
            UserDefaults.standard.user = user
        }
    }

    weak var delegate: LoginViewModelDelegate?

    func fetchUserDataFacebook(controller: LoginViewController) {
        let loginManager = LoginManager()

        loginManager.logIn(permissions: [.publicProfile, .email], viewController: controller) { result in

            switch result {
            case .failed(let error):
                self.delegate?.loginFailed(message: error.localizedDescription)

            case .success:
                let request = GraphRequest(graphPath: "me", parameters: ["fields": "email, name"])

                request.start { (_, result, error) in

                    if error == nil {
                        if let result = result as? [String: String?] {
                            if let email = result["email"],
                               let name = result["name"] {
                                self.user = User(name: name, email: email)
                                self.delegate?.didLoginSuccessfully()
                            }
                        }
                    } else {
                        self.delegate?.loginFailed(message: error?.localizedDescription)
                    }
                }
            case .cancelled:
                self.delegate?.loginFailed(message: Constant.facebookCancel)
            }
        }
    }

    func fetchUserDataGoogle(controller: UIViewController) {

        GIDSignIn.sharedInstance.signIn(with: Constant.signInConfig, presenting: controller) { user, error in

            if error == nil {
                if let userProfile = user {
                    let user = User(name: userProfile.profile?.name, email: userProfile.profile?.email)
                    self.user = user
                    self.delegate?.didLoginSuccessfully()
                }
            } else {
                self.delegate?.loginFailed(message: error?.localizedDescription)
            }
        }
    }

    func handleRegularLogin(password: String?, userName: String?) {
        if let password = password, let name = userName {
            if password.isValidPassword, name.isValidUsername  {
                let normalUser = User(name: userName, email: Constant.defaultEmail)
                self.user = normalUser
                self.delegate?.didLoginSuccessfully()
            } else {
                self.delegate?.loginFailed(message: "Debes llenar todos los campos")
            }
        }
    }
}
