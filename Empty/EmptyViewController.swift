//
//  EmptyViewController.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 23/5/22.
//

import UIKit

class EmptyViewController: TPAViewController {

    @IBOutlet private(set) weak var emptyLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        emptyLabel.text = "¡Bienvenido, usuario! Parece que la opcion del menu seleccionada no ha sido implementada aún en el proyecto. Prueba con otra opción."
        navigationItem.leftBarButtonItem?.tintColor = UIColor(named: "mainWhite")
    }
}
