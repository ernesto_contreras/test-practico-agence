//
//  ProductListViewController.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 21/5/22.
//

import UIKit

protocol ProductListViewModelDelegate: AnyObject {
    func productsFetchSucceeded()
    func productsFetchFailed(message: String?)
}

class ProductListViewController: TPAViewController {

    // MARK: - Outlets
    @IBOutlet private(set) weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    lazy var viewModel: ProductListViewModel = {
       let vm = ProductListViewModel()
        vm.delegate = self
        return vm
    }()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setup
    private func setup() {
        setupCollectionView()
        navigationItem.leftBarButtonItem?.tintColor = .darkGray
        hasMenu = true
        showActivityIndicator()
        viewModel.fetchImages()
    }

    private func setupCollectionView() {
        collectionView.register(
            UINib(nibName: ProductListCollectionViewCell.cellIdentifier, bundle: nil),
            forCellWithReuseIdentifier: ProductListCollectionViewCell.cellIdentifier
        )
        collectionView.register(
            UICollectionReusableView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: "collectionHeader"
        )
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension ProductListViewController: UICollectionViewDataSource,
                                     UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getTotalProducts()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ProductListCollectionViewCell.cellIdentifier, for: indexPath
        ) as? ProductListCollectionViewCell else {
            return UICollectionViewCell()
        }

        let product = viewModel.getProducts()[indexPath.row]
        cell.setupCell(name: product.name, image: product.image?.urls.thumb)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailVC = ProductDetailViewController()
        detailVC.product = viewModel.getProducts()[indexPath.row]
        self.navigationController?.pushViewController(detailVC, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 24, right: 16)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width / 2) - 24, height: UIScreen.main.bounds.height / 4)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "collectionHeader", for: indexPath)
            let label = UILabel(
                frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 24)
            )
            label.text = "Products list"
            label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            label.textColor = .darkGray
            reusableView.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                label.centerXAnchor.constraint(equalTo: reusableView.centerXAnchor),
                label.centerYAnchor.constraint(equalTo: reusableView.centerYAnchor)
            ])
            return reusableView
        } else {
            return UICollectionReusableView()
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 24)
    }
}

extension ProductListViewController: ProductListViewModelDelegate {
    func productsFetchSucceeded() {
        hideActivityIndicator()
        collectionView.reloadData()
    }
    
    func productsFetchFailed(message: String?) {
        hideActivityIndicator()
        let alertVC = TPAAlertViewController()
        alertVC.setAlert(titleMessage: message, type: .autoDismissable)
        self.present(alertVC, animated: true) {
            alertVC.dismissAlert()
        }
    }
}
