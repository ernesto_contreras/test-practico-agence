//
//  ProductListCollectionViewCell.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 23/5/22.
//

import UIKit
import Kingfisher

class ProductListCollectionViewCell: UICollectionViewCell {

    // MARK: - Constant
    static let cellIdentifier = String(describing: ProductListCollectionViewCell.self)
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var productImageView: UIImageView!
    @IBOutlet private(set) weak var productLabel: UILabel!
    
    // MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 24
    }
    
    // MARK: - Setup
    func setupCell(name: String, image: String?) {
        if let img = image, let url = URL(string: img) {
            productImageView.kf.indicatorType = .activity
            productImageView.kf.setImage(with: url, options: [.transition(.fade(0.2))])
        }
        productLabel.text = name
    }
}
