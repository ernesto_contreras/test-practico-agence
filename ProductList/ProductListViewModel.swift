//
//  ProductListViewModel.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 25/5/22.
//

import Foundation
import Alamofire

class ProductListViewModel {
    
    private var products = [Product]()
    weak var delegate: ProductListViewModelDelegate?
    
    func getProducts() -> [Product] {
        products
    }

    func getTotalProducts() -> Int {
        products.count
    }

    func fetchImages() {
        let headers: HTTPHeaders = [
            "Authorization" : "Client-ID JwVaAz0DMQ17nDWQwkdVU8KHD-sM0kdqkdQxD2vXJ14"
        ]
        let params: Parameters = [
            "count": 30,
            "orientation": "squarish"
        ]
        AF.request(Constant.unsplashURL, parameters: params, headers: headers).responseDecodable(of: [Image].self, completionHandler: { result in
            switch result.result {
            case .success(let images):
                for (index, image) in images.enumerated() {
                    self.products.append(
                        Product(name: "Producto \(index + 1)", image: image)
                    )
                }
                self.delegate?.productsFetchSucceeded()
            case .failure(let error):
                self.delegate?.productsFetchFailed(message: error.localizedDescription)
            }
        })
    }
}
