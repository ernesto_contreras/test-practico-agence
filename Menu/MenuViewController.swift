//
//  MenuViewController.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 23/5/22.
//

import UIKit
import SideMenuSwift

class MenuViewController: UIViewController {

    // MARK: - Outlet
    @IBOutlet private(set) weak var tableView: UITableView!
    @IBOutlet private(set) weak var coverView: UIView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Setup
    private func setup() {
        setupTableView()
        SideMenuController.preferences.basic.menuWidth = UIScreen.main.bounds.width
        sideMenuController?.delegate = self
        view.backgroundColor = .darkGray

        NotificationCenter.default.addObserver(self, selector: #selector(didBuyProduct(_:)), name: .didBuyProduct, object: nil)
    }
    private func setupTableView() {
        tableView.register(UINib(nibName: MenuTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: MenuTableViewCell.cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
    }

    // MARK: - Action
    @IBAction func menuButtonTapped(_ sender: Any) {
        sideMenuController?.hideMenu()
    }

    @objc private func didBuyProduct(_ notification: Notification) {
        let alertVC = TPAAlertViewController()
        alertVC.setAlert(titleMessage: Constant.productBought, type: .autoDismissable)
        self.present(alertVC, animated: true) {
            alertVC.dismissAlert()
        }
    }
}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuOption.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MenuTableViewCell.cellIdentifier, for: indexPath) as? MenuTableViewCell else {
            return UITableViewCell()
        }
        if let option = MenuOption(rawValue: indexPath.row) {
            cell.setupCell(option: option)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let option = MenuOption(rawValue: indexPath.row) else { return }
        switch option {
        case .myProducts:
            SceneSelector.shared.moveScene(controller: ProductListViewController.self)
        case .logout:
            UserDefaults.standard.user = nil
            SceneSelector.shared.setInitialScene()
        default:
            SceneSelector.shared.moveScene(controller: EmptyViewController.self)
        }
    }
}

extension MenuViewController: SideMenuControllerDelegate {
    func sideMenuControllerWillHideMenu(_ sideMenuController: SideMenuController) {
        coverView.isHidden = true
    }

    func sideMenuControllerWillRevealMenu(_ sideMenuController: SideMenuController) {
        coverView.isHidden = false
    }
}
