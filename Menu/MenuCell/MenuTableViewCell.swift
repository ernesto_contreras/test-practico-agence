//
//  MenuTableViewCell.swift
//  TestPracticoAgence
//
//  Created by Ernesto Jose Contreras Lopez on 23/5/22.
//

import UIKit

enum MenuOption: Int, CaseIterable {
    case profile = 0
    case myProducts = 1
    case settings = 2
    case logout = 3
}

class MenuTableViewCell: UITableViewCell {

    static let cellIdentifier = String(describing: MenuTableViewCell.self)

    @IBOutlet private(set) weak var optionImageView: UIImageView!
    @IBOutlet private(set) weak var optionLabel: UILabel!

    func setupCell(option: MenuOption) {
        switch option {
        case .profile:
            optionImageView.image = UIImage(named: "profile")
            optionLabel.text = "Perfil"
        case .myProducts:
            optionImageView.image = UIImage(named: "shopCar")
            optionLabel.text = "Mis productos"
        case .settings:
            optionImageView.image = UIImage(named: "settings")
            optionLabel.text = "Configuraciones"
        case .logout:
            optionImageView.image = UIImage(named: "logout")
            optionLabel.text = "Cerrar sesión"
        }
    }
}
